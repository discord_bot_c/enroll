# Enroll

It is a project to write a Discord Bot where we can use emojis to enroll for a specific role to an event.

## Getting started

To understand how a bot is first read here please : 
https://realpython.com/how-to-make-a-discord-bot-python/#creating-a-discord-connection


## Requirements 

```
Python 3.9
token.txt  --> your token should be here
```

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.
