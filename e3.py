import discord
import asyncio
from discord.ext import commands

# Define your intents
intents = discord.Intents.default()
intents.messages = True
intents.guilds = True
intents.message_content = True
intents.reactions = True  # Add this line

bot = commands.Bot(command_prefix='!', intents=intents)

# Your table data structure with custom emojis
table_data = {
    'row1': {'name': "F-16", 'emoji': '<:f16:1182241259792896062>','count': 0, 'users': []},
    'row2': {'name': "F-14", 'emoji': '<:f14:1182241242801782784>', 'count': 0, 'users': []},
    # Add more rows as needed
}

async def update_table():
    # Code to display the updated table
    # You can customize this part based on your needs
    table_message = '```Markdown\n'
    for row_key, row in table_data.items():
        table_message += f'{row["name"]}:\n{row["count"]} users clicked\n'
        if row["users"]:
            users_links = [f'[{user}]' for user in row["users"]]
            table_message += ', '.join(users_links) + '\n'
        else:
            table_message += 'No users\n'
        table_message += '\n'
    table_message += '```'

    # Send the message to the "table-channel"
    table_channel_name = 'yoklama'  # Change to your desired channel name
    table_channel = discord.utils.get(bot.get_all_channels(), name=table_channel_name)
    if table_channel:
        # Find the existing table message if it exists
        existing_messages = ""

        if existing_messages:
            # If there's an existing message, update it
            message = existing_messages[0]
            await message.edit(content=table_message)
        else:
            # If there's no existing message, send a new one
            message = await table_channel.send(table_message)

            # Add custom reactions to the message
            for row_key, row in table_data.items():
                await message.add_reaction(row['emoji'])
    else:
        print(f'Error: Could not find the channel named "{table_channel_name}".')

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} ({bot.user.id})')

    # Call the update_table function when the bot is ready
    asyncio.ensure_future(update_table())


@bot.event
async def on_reaction_add(reaction, user):
    # Ignore reactions by the bot
    if user == bot.user:
        return

            
    # Check if the reaction is on the correct message and channel
    if reaction.message.channel.name == 'yoklama':
        # Map emoji to row
        emoji_to_row = {row['emoji']: row_key for row_key, row in table_data.items()}

        # Check if the reaction emoji corresponds to a row
        if reaction.emoji in emoji_to_row:
            row_key = emoji_to_row[reaction.emoji]

            # Remove user from other rows
            for other_row_key, other_row in table_data.items():
                if other_row_key != row_key and user.display_name in other_row['users']:
                    other_row['users'].remove(user.display_name)

            # Update the table data
            table_data[row_key]['count'] += 1
            table_data[row_key]['users'].append(user.display_name)

            # Update the message with the modified table
            update_table()

# Run the bot
# Read the token from the file
with open("token.txt", "r") as token_file:
    bot_token = token_file.read().strip()
# Run the bot
bot.run(bot_token)
