import discord

from discord.ext import commands

intents = discord.Intents.default()
intents.reactions = True
intents.message_content = True  

bot = commands.Bot(command_prefix='!', intents=intents)

# Your table data structure with custom emojis
table_data = {
    'row1': {'name': "F-16", 'emoji': '<:f16:1182241259792896062>','count': 0, 'users': []},
    'row2': {'name': "F-14", 'emoji': '<:f14:1182241242801782784>', 'count': 0, 'users': []},
    # Add more rows as needed
}
table_channel_name = "yoklama"

def update_table():
    table_message = '```Markdown\n'
    for row_key, row in table_data.items():
        table_message += f'{row["name"]}:\n{row["count"]} users clicked\n'
        if row["users"]:
            users_links = [f'[{user}]' for user in row["users"]]
            table_message += ', '.join(users_links) + '\n'
        else:
            table_message += 'No users\n'
        table_message += '\n'
    table_message += '```'

    table_channel = discord.utils.get(bot.get_all_channels(), name=table_channel_name)
    if table_channel:
        existing_messages = table_channel.history(limit=1)

        if existing_messages:
            message = existing_messages[0]
            message.edit(content=table_message)
        else:
            message = table_channel.send(table_message)
            for row_key, row in table_data.items():
                message.add_reaction(row['emoji'])
    else:
        print(f'Error: Could not find the channel named "{table_channel_name}".')

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} ({bot.user.id})')

    # Call the update_table function when the bot is ready
    update_table()

@bot.event
async def on_reaction_add(reaction, user):
    if user.bot:
        return

    if reaction.message.channel.name == table_channel_name:
        emoji_to_row = {row['emoji']: row_key for row_key, row in table_data.items()}
        if reaction.emoji in emoji_to_row:
            row_key = emoji_to_row[reaction.emoji]

            # Remove user from other rows
            for other_row_key, other_row in table_data.items():
                if other_row_key != row_key and user.display_name in other_row['users']:
                    other_row['users'].remove(user.display_name)

            # Update the table data
            table_data[row_key]['count'] += 1
            table_data[row_key]['users'].append(user.display_name)

            # Update the message with the modified table
            await update_table()

# Read the token from the file
with open("token.txt", "r") as token_file:
    bot_token = token_file.read().strip()
# Run the bot
bot.run(bot_token)