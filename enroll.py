import discord
from discord.ext import commands

# Define your intents
intents = discord.Intents.default()
intents.messages = True
intents.guilds = True
intents.message_content = True

bot = commands.Bot(command_prefix='!', intents=intents)

# Your table data structure
table_data = {
    'row1': {'name': 'F-16', 'count': 0, 'users': []},
    'row2': {'name': 'F-15', 'count': 0, 'users': []},
    'row3': {'name': 'Coskun abi sen buraya tikla', 'count': 0, 'users': []},
    # Add more rows as needed
}

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} ({bot.user.id})')

@bot.command(name='showtable')
async def show_table(ctx):
    # Code to display the table
    # You can customize this part based on your needs
    table_message = '```Markdown\n'
    for row_key, row in table_data.items():
        table_message += f'{row["name"]}:\n{row["count"]} users clicked\n{", ".join(row["users"])}\n\n'
    table_message += '```'

    # Send the message to the "table-channel"
    table_channel = discord.utils.get(ctx.guild.channels, name='yoklama')
    if table_channel:
        await table_channel.send(table_message)
    else:
        await ctx.send(table_message)

@bot.command(name='click')
async def click_row(ctx, row_number: int):
    # Code to handle user clicks
    # You can customize this part based on your needs
    row_key = f'row{row_number}'
    if row_key in table_data:
        author_name = str(ctx.author)
        if author_name not in table_data[row_key]['users']:
            table_data[row_key]['count'] += 1
            table_data[row_key]['users'].append(author_name)
            await ctx.send(f'{author_name} clicked on {table_data[row_key]["name"]}')
            
            # Send the updated table to the "table-channel"
            table_channel = discord.utils.get(ctx.guild.channels, name='table-channel')
            if table_channel:
                await table_channel.send(f'{author_name} clicked on {table_data[row_key]["name"]}\n\n{table_message}')
            else:
                await ctx.send('Error: Could not find the specified channel.')
        else:
            await ctx.send(f'{author_name}, you already clicked on {table_data[row_key]["name"]}')
    else:
        await ctx.send(f'Invalid row number. Please choose a valid row.')

# Read the token from the file
with open("token.txt", "r") as token_file:
    bot_token = token_file.read().strip()
# Run the bot
bot.run(bot_token)
