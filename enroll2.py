import discord
from discord.ext import commands

# Define your intents
intents = discord.Intents.default()
intents.messages = True
intents.guilds = True
intents.message_content = True  # Add this line

bot = commands.Bot(command_prefix='!', intents=intents)

# Your table data structure
table_data = {
    'row1': {'name': 'Row 1', 'count': 0, 'users': []},
    'row2': {'name': 'Row 2', 'count': 0, 'users': []},
    # Add more rows as needed
}

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} ({bot.user.id})')

    # Code to display the table
    # You can customize this part based on your needs
    table_message = '```Markdown\n'
    for row_key, row in table_data.items():
        table_message += f'{row["name"]}:\n{row["count"]} users clicked\n'
        if row["users"]:
            users_links = [f'[{user}]' for user in row["users"]]
            table_message += ', '.join(users_links) + '\n'
        else:
            table_message += 'No users\n'
        table_message += '\n'
    table_message += '```'

    # Send the message to the "table-channel"
    table_channel_name = 'yoklama'  # Change to your desired channel name
    table_channel = discord.utils.get(bot.get_all_channels(), name=table_channel_name)
    if table_channel:
        message = await table_channel.send(table_message)

        # Add Unicode reactions to the message
        emoji_ids = ['<:f16:1182241259792896062>', '<:f14:1182241242801782784>']
        for emoji_id in emoji_ids:
            await message.add_reaction(emoji_id)
    else:
        print(f'Error: Could not find the channel named "{table_channel_name}".')

    # Close the bot
    await bot.close()

# Read the token from the file
with open("token.txt", "r") as token_file:
    bot_token = token_file.read().strip()
# Run the bot
bot.run(bot_token)
